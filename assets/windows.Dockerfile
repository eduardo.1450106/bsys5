FROM ubuntu:latest
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install bash git wget build-essential zip
RUN dpkg --add-architecture i386 && apt-get -y update && apt-get -y install wine32

RUN tmpdir=$(mktemp -d) && cd $tmpdir && git clone https://gitlab.com/librewolf-community/browser/windows.git && cd windows/linux && make setup-debian && make fetch && make bootstrap && cd /root && rm -rf $tmpdir

WORKDIR /work
VOLUME ["/work"]
